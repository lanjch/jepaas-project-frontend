import fetch from './fetch';
import {
  POST_GET_NO_READ_COUNT,
  POST_GET_MICROMAIL_TOTAL_COUNT,
  POST_GET_MICROMAIL_TOTAL_TYPE_COUNT,
  POST_GET_MICROMAIL_BY_ID,
  POST_GET_MICROMAIL_UPS_BY_ID,
  POST_GET_READ_MICROMAIL_LIST,
  POST_GET_NO_READ_MICROMAIL_LIST,
  POST_GET_MICROMAIL_COMMENTS,
  POST_TO_MICROMAIL_REMIND,
  POST_GET_MY_MICROMAIL_LISTS,
  POST_CREATE_MICROMAIL,
  POST_ADD_READ_MICROMAIL,
  POST_DELETE_MICROMAIL,
  POST_CREATE_COMMENT,
  POST_DELETE_COMMENT,
} from './api';

/*
 *获取微邮未读数量
 */
export function getNoreadCount(param) {
  return fetch(POST_GET_NO_READ_COUNT, { params: param }, { async: true })
    .then(data => data)
    .catch();
}

/*
 *获取微邮 全部数量
 */
export function getTotalCount(param) {
  return fetch(POST_GET_MICROMAIL_TOTAL_COUNT, { params: param }, { async: true })
    .then(data => data)
    .catch();
}

/*
 *获取微邮  各自不同tab的总数
 */
export function getTypeTotalCount(param) {
  return fetch(POST_GET_MICROMAIL_TOTAL_TYPE_COUNT, { params: param }, { async: true })
    .then(data => data)
    .catch();
}

/*
 *获取微邮详情
 */
export function getMailDetail(param) {
  return fetch(POST_GET_MICROMAIL_BY_ID, { params: param }, { async: true })
    .then(data => data)
    .catch();
}

/*
 *获取微邮已赞数据列表
 */
export function getZanlist(param) {
  return fetch(POST_GET_MICROMAIL_UPS_BY_ID, { params: param }, { async: true })
    .then(data => data)
    .catch();
}

/*
 *获取已阅列表
 */
export function getReadList(param) {
  return fetch(POST_GET_READ_MICROMAIL_LIST, { params: param }, { async: true })
    .then(data => data)
    .catch();
}

/*
 *获取未阅列表
 */
export function getUnreadList(param) {
  return fetch(POST_GET_NO_READ_MICROMAIL_LIST, { params: param }, { async: true })
    .then(data => data)
    .catch();
}

/*
 *获取评论列表
 */
export function getCommentList(param) {
  return fetch(POST_GET_MICROMAIL_COMMENTS, { params: param }, { async: true })
    .then(data => data)
    .catch();
}

/*
 *发送未阅提醒
 */
export function sendRemind(param) {
  return fetch(POST_TO_MICROMAIL_REMIND, { params: param }, { async: true })
    .then(data => data)
    .catch();
}

/*
 *获取我的微邮
 */
export function getMyMailList(param) {
  return fetch(POST_GET_MY_MICROMAIL_LISTS, { params: param }, { async: true })
    .then(data => data)
    .catch();
}

/*
 *创建我的微邮
 */
export function createMyMail(param) {
  return fetch(POST_CREATE_MICROMAIL, { params: param }, { async: true })
    .then(data => data)
    .catch();
}

/*
 *删除微邮
 */
export function deleteMyMail(param) {
  return fetch(POST_DELETE_MICROMAIL, { params: param }, { async: true })
    .then(data => data)
    .catch();
}

/*
 *评论微邮
 */
export function commentMail(param) {
  return fetch(POST_CREATE_COMMENT, { params: param }, { async: true })
    .then(data => data)
    .catch();
}
/*
 *删除评论
 */
export function deleteComment(param) {
  return fetch(POST_DELETE_COMMENT, { params: param }, { async: true })
    .then(data => data)
    .catch();
}

/*
 *添加微邮阅读记录
 */
export function addReadMicromail(param) {
  return fetch(POST_ADD_READ_MICROMAIL, { params: param }, { async: false })
    .then(data => data)
    .catch();
}
