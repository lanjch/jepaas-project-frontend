/*
 * @Description:
 * @version: V1.0.0
 * @Author: Shuangshuang Song
 * @Date: 2020-08-20 14:10:27
 * @LastEditTime: 2020-10-29 14:53:49
 * @LastEditors: Shuangshuang Song
 */
import { install } from '../../install.js';
import index from './index.vue';
// 编辑器
const js = [
  '/static/ux/editor/ueditor1_4_3/ueditor.JEconfig.js',
  '/static/ux/editor/ueditor1_4_3/ueditor.all.min.js',
  '/static/ux/editor/ueditor1_4_3/formdesign/leipi.form.design.js',
  '/static/ux/editor/ueditor1_4_3/lang/zh-cn/zh-cn.js ',
  // '/static/ux/moment/moment.min.js',
];
JE.loadScript(js);
// 安装组件
install('MicroMail', index);
