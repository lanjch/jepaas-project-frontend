/* eslint-disable class-methods-use-this */
/*
 * @Descripttion:
 * @Author: qinyonglian
 * @Date: 2019-11-11 15:39:57
 * @LastEditors  : qinyonglian
 * @LastEditTime : 2020-01-09 18:26:34
 */

// 数据模型
import DiskModel from '../../model/diskModel';
import {
  skydriveDownload,
  fileRename,
  floderList,
  floderQuery,
  fileCopy,
  fileMove,
  editorTags,
  flieRecycle,
  shareingFiles,
  folderByCreate,
  goMail,
  loadTree,
  getPreview,
  setFile,
  getDiskLog,
  getPowerFile,
  getUpLoadFiles,
} from '../../actions/action';
import { download } from '../../common/download';
import { isSysAdmin } from '../../common/util';

export default class List {
  // constructor(options) {

  // }
  async getLoadTree(params) {
    const res = await loadTree(params);
    return res;
  }

  async getPreview(nodeId, diskType) {
    const res = await getPreview({
      nodeId,
      diskType,
    });
    return res;
  }

  /*
   * @param {nodeIds} 需要传到微邮那边的IDs
   * @param {disktype} 网盘类型
   * @return:
   */
  async goMicroMail(nodeIds, diskType) {
    const res = await goMail({
      nodeIds: nodeIds.join(','),
      diskType,
    });
    return res;
  }

  /* 初始化文件或者公司的列表
   * @param {diskType} 网盘类型[self、company]
   * @param {nodeType} 文件类型[dir 、file]
   * @param {parentId} 父级目录id，无值默认一级目录
   * @param {tag} 标签
   * @param {suffix} 文件尾缀
   * @param {key} 搜索关键字
   * @param {order} 排序条件[{code:"time||size||name",type:"desc||asc"}]
   * @return:
   */


  async initList(params) {
    // 1.调取接口，渲染列表diskType, order, nodeType, parentId, tag, suffix, key
    // 2.数据模型
    // order = order || [{ code: 'time', type: 'desc' }];
    // const res = await floderList({
    //   diskType,
    //   nodeType,
    //   parentId,
    //   tag,
    //   suffix,
    //   key,
    //   order: JSON.stringify(order),
    // });
    const res = await floderList(params);
    return res && res.map(item => DiskModel.create(item));
  }

  /* 目录搜索接口
   * @param {nodeName} 目录名称
   * @param {nodeTag} 目录标签
   * @param {diskType} 网盘类型
   * @return:
   */
  static flodeQuery(nodeName, nodeTag, diskType) {
    const res = floderQuery({
      nodeName,
      nodeTag,
      diskType,
    });
    return res.map(item => DiskModel.create(item));
  }

  /* 获取个人文件或者公司文件的右侧二级列表
   * @param {secondMenu} 二级列表
   * @param {checkList} 选中的列表
   * @return:
   */
  getSecondMenu(secondMenu, checkList, fileData) {
    // 单独将公司文件的权限进行处理，个人文件不进行处理还是之前的处理方式
    if (secondMenu.router == '1-1') {
      const menuData = this.companySecondMenu(secondMenu, checkList, fileData);
      return menuData;
    }
    return secondMenu.menuData.filter((data, data_) => {
      if (checkList.length > 0) {
        data.status = true;
        // 前两个个新建和上传默认不关闭
      } else if (data_ > 1 && secondMenu.secondHeadStatus == '1') {
        data.status = false;
      } else if (secondMenu.secondHeadStatus == '2') {
        data.status = false;
      } else if (!['1-1', '1-2'].includes(secondMenu.router)) {
        data.status = false;
      }
      return data;
    });
  }

  /*
   * 公司文件路由进行单独处理
   * @param { Object } secondMenu 当前的公司文件的菜单
   * @param { Array } checkList 当前选中的文件数据
   * @returns
   * @memberof List
   */
  companySecondMenu(secondMenu, checkList, fileData) {
    // 双击进入文件之后的数据新建上传的显示
    if (fileData) {
      const fileStatus = ['manage', 'edit'].includes(fileData.roleRoleCode);
      secondMenu.menuData = secondMenu.menuData.filter((menu, index) => {
        fileStatus && index <= 1 && (menu.status = true);
        !fileStatus && index <= 1 && (menu.status = false);
        return menu;
      });
    }
    // 如果当前的fileData没有数据的时候说明是根目录，所以从新判断文件权限的文档权限(是否显示新建文件夹和上传功能)
    if (!fileData) {
      secondMenu.menuData = secondMenu.menuData.filter((menu, index) => {
        isSysAdmin() && index <= 1 && (menu.status = true);
        !isSysAdmin() && index <= 1 && (menu.status = false);
        return menu;
      });
    }
    // 如果没有选中的内容
    if (checkList.length == 0) {
      secondMenu.menuData = secondMenu.menuData.filter((menu, index) => {
        index > 1 && (menu.status = false);
        return menu;
      });
      return secondMenu.menuData;
    }
    // 如果有选中的内容则进行判断
    const roleIndex = checkList.findIndex(role => ['manage', 'edit'].includes(role.roleRoleCode));
    if (roleIndex == -1) {
      secondMenu.menuData = secondMenu.menuData.filter((menu, index) => {
        index > 1 && (menu.status = false);
        index == 2 && (menu.status = true);
        return menu;
      });
    } else {
      secondMenu.menuData = secondMenu.menuData.filter((menu, index) => {
        index > 1 && (menu.status = true);
        return menu;
      });
    }
    return secondMenu.menuData;
  }

  /* 添加文件夹
   * @param {parentId} 父节点id（如果无值的话默认在根目录下创建）
   * @param {folderName} 创建的文件夹名称
   * @param {diskType} 网盘类型
   * @return:
   */
  async addFolder(parentId, folderName, diskType) {
    // 调取接口，前端验证文件夹名称
    // 通过返回来的接口值unshift到_list中
    // 返回_list
    if (!folderName) {
      JE.msg('请输入文件夹名称');
      return false;
    }
    const res = await folderByCreate({
      parentId,
      folderName,
      diskType,
    });
    return res && DiskModel.create(res[0]);
  }

  /*
   * 修改文件夹名称
   * @param { diskType }网盘类型
   * @param { rename } 要修改成的内容
   * @param { nodeId } 需要修改名称的目录/文件id
   * @return:
   */
  async editFile(diskType, rename, nodeId) {
    const res = await fileRename({
      diskType,
      rename,
      nodeId,
    });
    return res;
    // 之后要做的处理本item时间和文件的名称都要修改 直接刷新列表
  }


  /*
   * 网盘下载
   * @param {dir}当前目录id路径
   * @param {file}文件对象
   * @return:
   */
  skyDriveDownLoad(nodeIds, diskType, fileName) {
    const res = skydriveDownload({
      nodeIds,
      fileName,
      diskType,
    });
    return res;
  }

  /* 文件复制
   * @param {nodeIds} 被操作目录或者文件id集合，使用英文逗号分隔
   * @param {targetNodeId} 操作目的目录
   * @param {diskType} 网盘类型[self、company]
   * @return:
   */
  async filesCopy(nodeIds, targetNodeId, diskType) {
    const res = await fileCopy({
      nodeIds: nodeIds.join(','),
      targetNodeId,
      diskType,
    });
    return res && DiskModel.create(res[0]);
  }


  /* 文件移动
   * @param {nodeIds} 被操作目录或者文件id集合，使用英文逗号分隔
   * @param {targetNodeId} 操作目的目录
   * @param {diskType} 网盘类型[self、company]
   * @return:
   */
  async filesMove(nodeIds, targetNodeId, diskType) {
    const res = await fileMove({
      nodeIds,
      targetNodeId,
      diskType,
    });
    return res;
  }

  /* 编辑tags
   * @param {nodeIds} 添加标签的目录/文件id
   * @param {tags} 要添加多个tags 多个用英文逗号隔开
   * @param {diskType} 网盘类型
   * @return:
   */
  async editFileTag(nodeIds, tags, diskType) {
    const res = await editorTags({
      nodeIds,
      tags,
      diskType,
    });
    return res;
    // 这边编辑成功之后，由于时间和tag都要变，以及相关联的数据都要进行改变，直接对页面进行刷新
  }

  /* 将文件放入回收站
   * @param {nodeIds}
   * @return:
   */
  async fileByRecycle(nodeIds, diskType) {
    const res = await flieRecycle({
      nodeIds: nodeIds.join(','),
      diskType,
    });
    return res;
  }

  /* 分享文件
   * @param {nodeIds} 分享的目录/文件id集合
   * @param {userIds} 被分享人id，多个使用逗号隔开
   * @return:
   */
  filesByShare(nodeIds, userIds, names) {
    shareingFiles({
      nodeIds: nodeIds.join(','),
      userIds: userIds.join(','),
      names: names.join(','),
    });
  }

  /*
  * 公司文件设置权限
  * @param {nodeId} 设置权限的文件id
  * @param {empowerPeople} 被选中的人员
  * @return:
  */
  setPowerFile(nodeId, empowerPeople) {
    setFile({
      nodeId,
      empowerPeople: JSON.stringify(empowerPeople),
    });
  }

  /*
   * 获取 操作历史记录
   * @param {nodeId}要查看操作记录的文件id
   * @return:
   */
  async getDiskLogs(nodeId) {
    const res = await getDiskLog({ nodeId });
    return res;
  }

  /*
   * 获取文件的权限
   * @param {nodeId}  文件的nodeId
   * @return:
   */
  async getPowerFiles(nodeId) {
    const res = await getPowerFile({ nodeId });
    return res;
  }
  /*
   * 获取拖拽文件的是否可以上传
   * @param {nodeId}  父级的nodeId
   * @param {disType} 文件类型 公司还是个人
   * @param {structure} 路径
   * @return:
   */
  async getUpFiles(nodeId, diskType, structure) {
    const res = await getUpLoadFiles({ nodeId, diskType, structure });
    return res;
  }


  static create() {
    return new List(...arguments);
  }
}


