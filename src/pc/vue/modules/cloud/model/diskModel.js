/*
 * @Descripttion:
 * @version:
 * @Author: qinyonglian
 * @Date: 2019-11-04 14:52:24
 * @LastEditors  : qinyonglian
 * @LastEditTime : 2019-12-20 15:48:12
 */
import BaseModel from './baseModel';
import { companyIconRole, operationRole } from '../common/util';

export default class DiskModel extends BaseModel {
  constructor(option) {
    super(option); // 相当于获得父类的this指向,继承属性

    this._init(option);
  }

  _init(option) {
    // 如果是自己的私有属性
    // 创建者
    this.createUser = option.createUserName;
    // 操作是分享
    this.operation = operationRole(option.roleRoleCode, option.diskType, option);
    // 选中的文件 或者文件夹有哪些操作功能
    this.docFunc = companyIconRole(option.roleRoleCode, option);
    // 公司文件的管理权限和操作记录的参数
    this.roleCreateTime = option.roleCreateTime || null;
    this.roleCreateUser = option.roleCreateUser || null;
    this.roleCreateUserName = option.roleCreateUserName || null;
    this.roleId = option.roleModifiedTime || null;
    this.roleModifiedUser = option.roleModifiedUser || null;
    this.roleModifiedUserName = option.roleModifiedUserName || null;
    this.roleRelId = option.roleRelId || null;
    this.roleRelName = option.roleRelName || null;
    this.roleRoleCode = option.roleRoleCode || null;
    this.roleRoleId = option.roleRoleId || null;
    this.roleType = option.roleType || null;
    this.version = option.version || null;
  }

  /*
   * 创建对象
   */
  static create(options) {
    return new DiskModel(options);
  }
}
