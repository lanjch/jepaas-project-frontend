const lang = JE.getCookies('je-local-lang') || 'zh_CN';
const spe = {
  '操作频繁，请五分钟后再试': 'Operation is frequent. Please try again after five minutes',
  '已达到最大上传个数，请删除后在进行上传': 'The maximum number of uploads has been reached. Please delete it before uploading',
  '收件人不能为空，请选择!': 'The recipient cannot be empty, please select!',
  '主题不能为空，请输入!': 'Theme cannot be empty, please enter!',
  '正文不能为空，请输入!': 'Body cannot be empty, please enter!',
  '发送成功!': 'Success!',
  '发送失败!': 'Unsuccess!',
};

function returnMsg(cn) {
  if (!cn) return;
  if (lang != 'zh_CN') {
    return spe[cn];
  }
  return cn;
}

export default returnMsg;
