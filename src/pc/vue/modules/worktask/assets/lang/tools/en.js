const m = {
  tijiaotongxunlu: 'Address book submission',
  dept: 'Dept',
  name: 'Name',
  search: 'Search',
  unknown: 'Unknown',
  tijiaobeiwanglu: 'Submit the memo',
  save: 'Save',
  del: 'Delete',
  notes: 'Notes',
  addnotes: 'Add notes',
  tijiaoliucheng: 'Submission process',
  log: 'Log',
  memo: 'Memo',
  process: 'Process',
  contact: 'Contacts',
};
export default m;
