const m = {
  '1001': {
    'cn1': '账号或者密码错误超过[',
    'cn2': ']次,请使用其他方式登录!',
    'en1': 'Account or password error more than [',
    'en2': '] times, please use other way to log in!',
  },
  '1002': {
    'cn1': '账号或密码错误,您还剩余[',
    'cn2': ']次个人密码登录机会!',
    'en1': 'Account number or password error, you still have left [',
    'en2': '] personal password login opportunity!',
  },
  '1003': {
    'cn': '验证码错误!',
    'en': 'Verification code error!',
  },
  '1004': {
    'cn': '用户未认证!',
    'en': 'User not authenticated!',
  },
  '1005': {
    'cn': '认证失败!',
    'en': 'Authentication failed!',
  },
  '1006': {
    'cn': '帐号或密码错误!',
    'en': 'Wrong account or password!',
  },
  '1007': {
    'cn': '状态机不对，请重新登录!',
    'en': 'The state machine is not correct, please login again!',
  },
};
export default m;
