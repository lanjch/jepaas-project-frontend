/**
 * 自定义功能的视图
 */
Ext.define("PRO.demo.view.CenterView", {
	extend: 'Ext.panel.Panel',
	/**
	 * 声明面板的xtype
	 * 确保全局唯一，widget.是系统必带的前缀
	 * 此类xtype的引用为：test.centerview
	 * @type String
	 */
	alias: 'widget.demo.centerview',
	layout:'fit',
	// loadData:function(pdid){
	// 	var me=this;
	// 	me.update('<iframe scrolling="auto" frameborder="0" width="100%" height="100%" src="/pro/busflow/html/busimage.html?pdid='+me.pdid+'"></iframe>');
	// },
	initComponent: function(){
		var me = this;
		//me.html='<iframe scrolling="auto" frameborder="0" width="100%" height="100%" src="/pro/busflow/html/busimage.html?pdid='+me.pdid+'"></iframe>';
    	var tableViewCols = 6;//列数，1-12
    	me.store = JE.getStore({tableName:'JE_CORE_ENDUSER',pageSize:30,autoLoad:true});
    	//数据视图
    	me.items = [{
    		xtype:'dataview',
    		itemId:'view',
    		store:me.store,
    		autoScroll:true,
    		cls:'je-list-view',
    		overItemCls:'je-list-item-over',
    		selectedItemCls:'je-list-item-selected',
    		//以上样式都是系统自带的，也可以自定义
    		itemSelector: 'div.je-list-item-wrap',
    		emptyText:'<div style="padding:20px;text-align:center;color:#444;font-size:20px;">没有找到数据</div>',
    		allowDeselect:true,
    		multiSelect:true,//多选
    		plugins:[Ext.create('JE.core.ux.DragSelector', {})],//选取数据插件
			// listeners:{
			// 	selectionchange:function(selM,sels){
			// 		var dataview=selM.view.panel;
			// 		JE.msg("选择改变了");
			// 		JE.log(sels);
			// 	}
			// },//注册事件
    		tpl: [
    			'<tpl for=".">',
					'<div class="je-list-item-wrap je-col-'+tableViewCols+'" style="padding:10px;">',
						'<div>' ,
							'{[this.getRowNum(xindex)]}.{USERNAME}' ,
						'</div>',
						'<div style="padding:10px 0px;">' ,
							'<input style="margin-right:10px;cursor: pointer;" type="button" class="je-button-icon <tpl if="GENDER==\'WOMAN\'">JE_CORE_NVREN4516<tpl else>JE_CORE_NANREN1216</tpl>" />' ,
							'{BACKUSERCODE}' ,
						'</div>',
					'</div>',
    			'</tpl>',
    			{
    				getRowNum:function(xindex){
    					//当前页码
			    		var page = me.store.currentPage || 1,count = me.store.pageSize;
			    		//行号
			    		var xrownum = (page-1)*count+xindex;
			    		return xrownum;
    				}
    			}
    		]
    	}];
    	//功能按钮
    	me.tbar = {
    		cls:'je-button-bar',
    		items:[{
    			text:'获取选中的数据',
    			iconCls:'fa fa-user',
				// listeners:{
				// 	click:function(btn){
				//
				// 	}
				// },
    			handler:function(btn){
    				//活动数据视图
		    		var view = me.getCmp('view');
		    		//获取选中的数据
		    		var sels = view.getSelectionModel().getSelection();
		    		//获取选中人的名称
		    		var names = [];
		    		Ext.each(sels,function(sel){
		    			names.push(sel.get('USERNAME'));
		    		});
		    		if(JE.isEmpty(names)){
		    			JE.alert('请选择人员！');
		    		}else{
		    			JE.alert(names.join(','));
		    		}
	    		}
	    	}]
	    	
    	};
    	//分页，平台插件
		me.bbar = {
			xtype: 'uxpagingtoolbar',
			itemId:'pagingtoolbar',
			useTheme:true,
		    store: me.store
		}
        me.callParent(arguments);
        
        //注册单击事件，也可以在声明view时，绑定listeners属性注册事件
        me.getCmp('view').on('itemclick',function(view,record, item, index, e){
        	var name = record.get('USERNAME');
        	//简单示例，可以点击图标进行操作
        	var icon = e.getTarget('.je-button-icon');
        	if(icon){
        		JE.alert('您点击了'+name+'的图标！');
        		return;
        	}
        	
        	//正常单击操作
        	JE.msg(record.get('USERNAME'));
        });
    }
});