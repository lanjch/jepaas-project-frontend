/*
 * @Descripttion:
 * @Author: 张明尧
 * @Date: 2021-02-02 14:37:21
 * @LastEditTime: 2021-02-02 15:37:32
 */
/**
 * @Author : ZiQin Zhai
 * @Date : 2019/6/19 14:53
 * @Version : 1.0
 * @Last Modifined by : ZiQin Zhai
 * @Last Modifined time : 2019/6/19 14:53
 * @Description 登录相关的路由菜单
 * */
import createRouter from '../../util/RouterModel';
import template from './index.vue';
import index from './views/login/index.vue';
import resetPwd from './views/setNewPwd/index.vue';
import forgetPwd from './views/forgetPwd/index.vue';
import registerLogin from './views/register_login/index.vue';

const LoginRouter = [
  createRouter({
    path: '/JE-PLUGIN-LOGIN',
    name: 'login-wrapper',
    component: template,
    children: [
      createRouter({
        path: '',
        name: 'login',
        meta: {
          white: true,
        },
        component: index,
      }),
      createRouter({
        path: 'Reset_pwd',
        name: 'ResetPwd',
        meta: {
          title: '重置密码',
          white: true,
        },
        component: resetPwd,
      }),
      createRouter({
        path: 'forget_pwd',
        name: 'forgetPwd',
        meta: {
          title: '忘记密码',
          release: true,
          white: true,
        },
        component: forgetPwd,
      }),
      createRouter({
        path: 'register_login',
        name: 'forgetPwd',
        meta: {
          title: '忘记密码',
          release: true,
          white: true,
        },
        component: registerLogin,
      }),
    ],
  }),
];
export default LoginRouter;
