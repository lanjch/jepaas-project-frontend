// import fetch from '@/helper/httpUtil';
import { WORK_START_LISTDATA } from './api';

/**
 *获取新闻公告信息
 * @param param
 * @returns {Promise<T | never>}
 */
export function getWorkStart(param) {
  return JE.ajax({ url: WORK_START_LISTDATA, param })
    .then(data => data)
    .catch();

  // return  fetch(WORK_START_LISTDATA, {}, {
  //   type: 'post',
  //   data: param,
  // })
  //   .then(data => data)
  //   .catch();
}
