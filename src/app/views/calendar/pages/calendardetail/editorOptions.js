const options = {
  modules: {
    toolbar: [],
  },
  placeholder: '',
  readOnly: false,
};

export default options;
