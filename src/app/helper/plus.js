
/**
 * 打开新窗口
 * @param {string} url 要打开的页面地址
 * @param {string} id 指定页面ID 如果没指定 则使用url作为id
 * @param {object} options 可选:参数,等待,窗口,显示配置{params:{},waiting:{},styles:{},show:{}}
 * @param {object} handleHeadClick  启用原生头监听, 在对应页面添加 监听，符合条件就会触发回调
 *    支持配置监听位置
 *        {
 *          center:true // 监听中间区域 40%~80%
 *        }
 *    亦可支持同时配置center、right
 *
 *    // 对应页面添加如下监听
 *    window.addEventListener('handleHeadClick', () => {
 *      // Anything...
 *    });
 * @returns {webview}
 */
export function openWindow({
  url, id = url, title = '', type = 'default', hideNView = false,
  styles: {
    statusbar = '#536DFE', popGesture = 'close', ...otherStyle
  } = {},
  handleHeadClick = {},
  extras = {},
  ...other
}) {
  if (!window.plus && _vue) {
    const childPath = url.split('_')[1];
    _vue.$router.push({ path: `/${_vue.$route.meta.pluginCode}/${childPath}`, query: { ...extras } });
    return false;
  }

  const styles = {
    // 沉浸式栏底色
    statusbar: {
      background: statusbar,
    },
    // iOS侧滑
    popGesture,
    titleNView: !hideNView && { // 窗口的标题栏控件
      type, // 标题栏控件样式
      titleText: title, // 标题栏文字,当不设置此属性时，默认加载当前页面的标题，并自动更新页面的标题
      titleSize: '17px', // 字体大小,默认17px
      titleColor: '#fff', // 字体颜色,颜色值格式为"#RRGGBB",默认值为"#000000"
      autoBackButton: true,
      backgroundColor: '#536dfe', // 控件背景颜色,颜色值格式为"#RRGGBB",默认值为"#F7F7F7"
      // progress: { // 标题栏控件的进度条样式
      //   color: '#F02619', // 进度条颜色,默认值为"#00FF00"
      //   height: '2px', // 进度条高度,默认值为"2px"
      // },
      // splitLine: { // 标题栏控件的底部分割线，类似borderBottom
      //   color: '#CCCCCC', // 分割线颜色,默认值为"#CCCCCC"
      //   height: '1px', // 分割线高度,默认值为"2px"
      // },
    },
    softinputMode: 'adjustResize',
    ...otherStyle,
  };
  const option = {
    styles, extras, ...other,
  };
  const webview = mui.openWindow({
    url,
    id,
    show: {
      aniShow: 'pop-in',
    },
    ...option,
  });

  // 处理点击原生头
  if (!JE.isEmpty(handleHeadClick)) {
    // 不支持监听left，左侧事件已经被back监听。
    const { center, right } = handleHeadClick;
    const innerWidth = plus.screen.resolutionWidth;
    webview.getTitleNView().addEventListener('click', ({ clientX }) => {
      function checkPosition() {
        switch (true) {
          case clientX < 0:
            return false;
          case center && clientX >= parseInt(innerWidth * 0.2, 10) && clientX <= parseInt(innerWidth * 0.8, 10):
            if (!right) { // 支持同时配置 center、right
              return true;
            }
          // eslint-disable-next-line no-fallthrough
          case right && clientX >= parseInt(innerWidth * 0.8, 10):
            return true;
          default:
            return false;
        }
      }
      // 配置title，right
      const check = checkPosition();
      if (check) {
        mui.fire(webview, 'handleHeadClick');
      }
    });
  }
  return webview;
}
/**
 * 图片预览
 * @param {*} imgs 图片地址
 * @param {*} options 配置项
 */
export function previewImage(imgs, options) {
  plus.nativeUI.previewImage(imgs, options);
}

export function refresh(callback) {
  const currentWebObj = plus.webview.currentWebview();
  currentWebObj.setPullToRefresh({
    support: true,
    height: '50px',
    range: '200px',

    contentdown: {
      caption: '下拉可以刷新',
    },
    contentover: {
      caption: '释放立即刷新',
    },
    contentrefresh: {
      caption: '正在刷新...',
    },
  }, callback);
}

/**
 *
 *  拍摄图片
 *
 * @export
 * @param {*} { path }
 * @returns {Promise }
 */
export function capture() {
  return new Promise((resolve, reject) => {
    const cmr = plus.camera.getCamera();
    cmr.captureImage((file) => {
      resolve(file);
    }, (e) => { reject(e); });
  });
}
/**
 *
 *  选择相册图片
 * @param {multiple} false 是否多选
 * @param {maximum}  9 最多可选几张（默认1张如果multiple为true 默认5张 也可以自定义)
 * @returns {Promise <Array>}
 */
export function pick({ multiple = false, maximum = multiple ? 9 : 1 } = {}) {
  return new Promise((resolve, reject) => {
    plus.gallery.pick((path) => {
      const { files } = path;
      resolve(files || [path]);
    }, (e) => {
      reject(e);
    }, {
      maximum,
      system: false,
      multiple,
      onmaxed() {
        plus.nativeUI.toast(`最多只能选择${maximum}张图片`);
      },
    });
  });
}
/**
 *
 *  压缩图片
 * @param {String} path 图片位置
 * @returns {Promise }
 */
export function zipImg(path) {
  return new Promise((resolve, reject) => {
    plus.zip.compressImage({
      src: path,
      dst: `_doc/temp/${+new Date()}_${Math.floor(Math.random() * 10000)}.jpg`,
      quality: 70,
    },
    ({ target }) => {
      resolve(target);
    },
    (e) => {
      JE.msg(e.message);
      reject(e);
    });
  });
}
/**
 * 上传图片
 * @param {Object} config 配置信息
 * @returns {Promise}
 */
export function uploaderImg({ path, hideWaiting = false, params = {} }) {
  function uploader(item) {
    return new Promise((resolve, reject) => {
      const task = plus.uploader.createUpload(JE.buildURL(`${JE.getUrlMaps('je.core.document.file')}?login=TRUE`),
        { method: 'POST', blocksize: 204800, priority: 100 },
        (t, status) => {
        // 上传完成
          if (status == 200) {
            resolve(t);
          } else {
            reject(t);
          }
        });
      task.addFile(item, { key: 'files' });
      task.addData('filePath', '/filestem');
      task.addData('filePath', "{ uploadType: 'TEMP' }");
      // 增加参数
      JE.each(params, (val, key) => {
        task.addData(val, key);
      });
      task.setRequestHeader('authorization', JE.getLSItem('authorization'));
      // task.addEventListener( "statechanged", onStateChanged, false );
      task.start();
    });
  }
  let wait;
  if (!hideWaiting) {
    wait = plus.nativeUI.showWaiting('正在上传');
  }
  return new Promise(async (resolve, reject) => {
    // 图片支持单选 多选
    const file = Array.isArray(path) ? path : [path];
    // fileArray
    const zipedFile = await Promise.all(file.map(item => zipImg(item)));
    Promise.all(zipedFile.map(item => uploader(item)))
      .then((result) => { resolve(result); })
      .catch((e) => { reject(e); })
      .then(() => { wait && wait.close(); });
  });
}
